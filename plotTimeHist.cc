
const double maxtime = 0.698;

void makeHist(TH1F* h, TFile* f)
{
    TNtuple* ntuple = (TNtuple*)f->Get("scint");

    double timeOfPhoton;        
    double atLG;        

	ntuple->SetBranchAddress("timeOfPhoton",  &timeOfPhoton);
	ntuple->SetBranchAddress("atLG",  &atLG);


    for(int i=0; i<ntuple->GetEntries(); i++)
    {
        ntuple->GetEntry(i);
        
		//if(timeOfPhoton<10. && atLG == 1.) h->Fill(timeOfPhoton);
		if(timeOfPhoton<maxtime && atLG == 1.) h->Fill(timeOfPhoton);
    }

	//h->Scale(1./h->Integral());

	cout << "nPhotons: " << h->GetEntries() << endl;
}

Double_t GetStartTime(const TH1F* h)
{
	double st = 0.;

	for(int i = 1; i <= h->GetNbinsX(); ++i)
	{
		if(h->GetBinContent(i)>0)
		{
			st = h->GetBinCenter(i);
			break;
		}
	}

	return st;
}

void plotTimeHist()
{
	TH1F* hTime1 = new TH1F("t1", "t1", 100, 0.65, maxtime);
	TH1F* hTime2 = new TH1F("t2", "t2", 100, 0.65, maxtime);

	TFile* f1 = new TFile("build/muonData_1cm.root");
	makeHist(hTime1, f1);

	TFile* f2 = new TFile("build/muonData_10cm.root");
	makeHist(hTime2, f2);

	hTime1->SetLineColor(kRed);
	hTime1->Draw();
	hTime2->SetLineColor(kGreen);
	hTime2->Draw("same");
	TF1* fun1 = new TF1("f1", "pol3", GetStartTime(hTime1), maxtime);
	hTime1->Fit("f1", "R");

	TF1* fun2 = new TF1("f2", "pol3", GetStartTime(hTime2), maxtime);
	hTime2->Fit("f2", "R");

	fun1->SetLineColor(kRed);
	fun2->SetLineColor(kGreen);
	fun2->Draw("same");
	fun1->Draw("same");

#if 0
	TCanvas* can = new TCanvas("", "", 1200, 800);
	can->Divide(2,1);
	can->cd(1);
	h->Draw();
	can->cd(2);
	h1->SetLineColor(kGreen);
	h1->Draw();
	can->Draw();
	cout << h << ", " << h1 << endl;
#endif
}
