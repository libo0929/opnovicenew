TH1F* hTime = new TH1F("", "", 1000, 0, 10);
	
TF1 *fun1 = new TF1("f1", "pol3", 0.95, 1.2);

const double area = 1./100; //1./(nbins/range) ???

Double_t IntdNdt(Double_t t)
{
	double integral = 0.;

	if(t < 3.9)
	{
		integral = fun1->Integral(0.95, t);
	}
	return integral/area;
}

Double_t Sig(Double_t *t, Double_t *para)
{
	Double_t vt = t[0]/1000; // ps2ns
	Double_t p = IntdNdt(vt);

	Double_t N = para[0];

	int  Nthr = 2;

    Double_t s;

	double prob;

	if(vt < 3.9)
		prob = fun1->GetFormula()->Eval(vt);

	prob = prob / area;

	if(Nthr == 2)
	{
		s = N * prob * (N - 1) *
		         //TMath::Power(p, Nthr - 1) * TMath::Power(1. - p, N - Nthr);
		         p * TMath::Power(1. - p, N - Nthr);
		//cout << vt << ", " << N << ", " << prob << ", " << p << ", " << s << endl;
	}
	else
	{
		s = N * prob * 
			     TMath::Power(N, Nthr - 1) / TMath::Factorial(Nthr-1) * 
		         TMath::Power(p, Nthr - 1) * TMath::Power(1. - p, N - Nthr);
	}

	return s * (1./1000); // 1./(nbins) ???
}

void timeResolutionData()
{
	TFile* f = new TFile("build/muonDet.root");
    TNtuple* ntuple = (TNtuple*)f->Get("scint");

    double timeOfPhoton;        
    double atLG;        

	ntuple->SetBranchAddress("timeOfPhoton",  &timeOfPhoton);
	ntuple->SetBranchAddress("atLG",  &atLG);


    for(int i=0; i<ntuple->GetEntries(); i++)
    {
        ntuple->GetEntry(i);
        
		if(timeOfPhoton<10. && atLG == 1.) hTime->Fill(timeOfPhoton);
    }

	hTime->Scale(1./hTime->Integral());

	cout << "n: " << hTime->GetEntries() << ", inte: " << hTime->Integral() << endl;

	hTime->Draw();
	hTime->Fit("f1", "R");
	fun1->SetLineColor(kGreen);
	fun1->SetLineWidth(5);
	fun1->Draw("same");

	double xmin = 950;
	double xmax = 1190;

	TF1 *f1 = new TF1("", Sig, xmin, xmax, 1);
	f1->SetNpx(1000);
	f1->SetParameter(0, 40000);
	f1->GetXaxis()->SetTitle("time (ps)");
	f1->GetYaxis()->SetTitle("probability");
	f1->Draw();

	TF1 *f2 = new TF1("", Sig, xmin, xmax, 1);
	f2->SetParameter(0, 10000);
	f2->SetNpx(1000);
	f2->SetLineColor(kBlue);
	f2->Draw("same");

	TF1 *f3 = new TF1("", Sig, xmin, xmax, 1);
	f3->SetNpx(1000);
	f3->SetParameter(0, 1000);
	f3->SetLineColor(kMagenta);
	f3->Draw("same");

	cout << f1->Integral(xmin, xmax) << ", " << f2->Integral(xmin, xmax) << ", " << f3->Integral(xmin, xmax) << endl;
}
