{
	Double_t myfunction(Double_t *t, Double_t *p)
	{
		//const double TMAX = 1.4; // ns -> 50 cm
		const double TMAX = 0.193; // the time for 100MeV, mu, 5 cm
		const int NP = 1000;
		const double step = TMAX/NP;

		Float_t time = t[0];

		Double_t f = 0;

		for(double ti = 0; ti < TMAX; ti += step)
		{
			if(ti < time) 
			{
				f += TMath::Exp(-(time - ti)/1.8) * (1./1794.45); // 1.8 ns for the scintillator, and the norm factor
			}
		}

		return f;
	}

	gStyle->SetOptStat(0);

	TF1 *f1 = new TF1("", myfunction, 0, 10);
	f1->SetNpx(1000);
	f1->GetXaxis()->SetTitle("photon time (ns)");
	f1->GetYaxis()->SetTitle("photon number");
	f1->Draw();

	//-------------------------------------- with data
	// 100 evts, 5cm, no CHKOV
	TFile* f = new TFile("build_testShape/muonData_5cm.root");
	TTree* scint = (TTree*)f->Get("scint");

	scint->Draw("timeOfPhoton>>ht(200, 0, 5)", "atLG==0&&timeOfPhoton<5");
	ht->SetTitle("");
	ht->GetXaxis()->SetTitle("photon time (ns)");
	ht->GetYaxis()->SetTitle("dN/dt (1/ns)");
	ht->Scale(1/ht->Integral());
	ht->Scale(37.5);
	ht->SetMarkerColor(600);
	ht->SetLineColor(600);
	ht->SetLineWidth(3);

	f1->SetLineWidth(2);
	f1->SetLineStyle(1);
	f1->Draw("same");

	ht->Draw("same");

    TLegend *legend = new TLegend(.55,.60,.75,.75);
    legend->AddEntry(f1, "Theoretical", "l");
    legend->AddEntry(ht, "G4 simulation", "l");
    legend->Draw();
	
	//cout << f1->Integral(0, 10) << endl;
}
