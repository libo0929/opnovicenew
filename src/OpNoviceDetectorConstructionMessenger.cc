//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
/// \file OpNovice/src/OpNoviceDetectorConstructionMessenger.cc
/// \brief Implementation of the OpNoviceDetectorConstructionMessenger class
//
//
//
// 

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "OpNoviceDetectorConstructionMessenger.hh"

#include "OpNoviceDetectorConstruction.hh"
#include "G4UIdirectory.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4SystemOfUnits.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

OpNoviceDetectorConstructionMessenger::
  OpNoviceDetectorConstructionMessenger(OpNoviceDetectorConstruction* OpNoviceDCA)
  : G4UImessenger(),
    fOpNoviceDetectorConstruction(OpNoviceDCA)
{
  fGunDir = new G4UIdirectory("/OpNovice/det/");
  fGunDir->SetGuidance("DetectorConstruction control");

  fThicknessCmd = new G4UIcmdWithADoubleAndUnit("/OpNovice/det/scintThickness", this);
  fThicknessCmd->SetGuidance("Set the scintillator thickness");
  fThicknessCmd->SetParameterName("thickness",true);
  fThicknessCmd->SetUnitCategory("Length");
  fThicknessCmd->SetDefaultValue(5);
  fThicknessCmd->SetDefaultUnit("cm");
  fThicknessCmd->AvailableForStates(G4State_Idle);

  fWidthCmd = new G4UIcmdWithADoubleAndUnit("/OpNovice/det/scintWidth", this);
  fWidthCmd->SetGuidance("Set the scintillator width");
  fWidthCmd->SetParameterName("width",true);
  fWidthCmd->SetUnitCategory("Length");
  fWidthCmd->SetDefaultValue(10);
  fWidthCmd->SetDefaultUnit("cm");
  fWidthCmd->AvailableForStates(G4State_Idle);

  fLengthCmd = new G4UIcmdWithADoubleAndUnit("/OpNovice/det/scintLength", this);
  fLengthCmd->SetGuidance("Set the scintillator length");
  fLengthCmd->SetParameterName("length",true);
  fLengthCmd->SetUnitCategory("Length");
  fLengthCmd->SetDefaultValue(25);
  fLengthCmd->SetDefaultUnit("cm");
  fLengthCmd->AvailableForStates(G4State_Idle);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

OpNoviceDetectorConstructionMessenger::~OpNoviceDetectorConstructionMessenger()
{
  delete fThicknessCmd;
  delete fWidthCmd;
  delete fLengthCmd;

  delete fGunDir;
}


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void OpNoviceDetectorConstructionMessenger::SetNewValue(
                                        G4UIcommand* command, G4String newValue)
{
  if( command == fThicknessCmd ) {
      G4double th = fThicknessCmd->GetNewDoubleValue(newValue);
      if ( th <= 0. * cm ) {
         fOpNoviceDetectorConstruction->SetThickness(0.1 * cm);
      } else {
         fOpNoviceDetectorConstruction->SetThickness(th);
      }
  }

  if( command == fWidthCmd ) {
      G4double wd = fWidthCmd->GetNewDoubleValue(newValue);
      if ( wd <= 0. * cm ) {
         fOpNoviceDetectorConstruction->SetWidth(0.1 * cm);
      } else {
         fOpNoviceDetectorConstruction->SetWidth(wd);
      }
  }

  if( command == fLengthCmd ) {
      G4double len = fLengthCmd->GetNewDoubleValue(newValue);
      if ( len <= 0. * cm ) {
         fOpNoviceDetectorConstruction->SetLength(0.1 * cm);
      } else {
         fOpNoviceDetectorConstruction->SetLength(len);
      }
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
