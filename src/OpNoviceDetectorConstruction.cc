//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
/// \file OpNovice/src/OpNoviceDetectorConstruction.cc
/// \brief Implementation of the OpNoviceDetectorConstruction class
//
//
//
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "OpNoviceDetectorConstruction.hh"

#include "G4Material.hh"
#include "G4Element.hh"
#include "G4LogicalBorderSurface.hh"
#include "G4LogicalSkinSurface.hh"
#include "G4OpticalSurface.hh"
#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4PVPlacement.hh"
#include "G4SystemOfUnits.hh"
#include "G4NistManager.hh"
#include "G4RunManager.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
G4double OpNoviceDetectorConstruction::fScint_x = 0.10 * m; // width
G4double OpNoviceDetectorConstruction::fScint_y = 0.05 * m; // thichness
G4double OpNoviceDetectorConstruction::fScint_z = 0.25 * m; // length

OpNoviceDetectorConstruction::OpNoviceDetectorConstruction()
 : G4VUserDetectorConstruction()
{
  fDCMessenger = new OpNoviceDetectorConstructionMessenger(this);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

OpNoviceDetectorConstruction::~OpNoviceDetectorConstruction(){;}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VPhysicalVolume* OpNoviceDetectorConstruction::Construct()
{
  // world size
  fExpHall_x = 1.2 * fScint_x;
  fExpHall_y = 1.2 * fScint_y;
  fExpHall_z = 1.2 * fScint_z + 0.02 * cm; // 0.02 cm -> double light guide length

// ------------- Materials -------------

  G4double a, z, density;
  G4int nelements;

// Air
//
  G4Element* N = new G4Element("Nitrogen", "N", z=7 , a=14.01*g/mole);
  G4Element* O = new G4Element("Oxygen"  , "O", z=8 , a=16.00*g/mole);

  G4Material* air = new G4Material("Air", density=1.29*mg/cm3, nelements=2);
  air->AddElement(N, 70.*perCent);
  air->AddElement(O, 30.*perCent);

  G4NistManager* nist = G4NistManager::Instance();
  G4Material* sc = nist->FindOrBuildMaterial("G4_PLASTIC_SC_VINYLTOLUENE");
  G4Material* lg_mat = nist->FindOrBuildMaterial("G4_GLASS_PLATE"); // FIXME

//
// ------------ Generate & Add Material Properties Table ------------
//
//
// plastic scintillator
//
  const G4int NUMENTRIES = 121;
  G4double energy[NUMENTRIES];
  G4double rindex[NUMENTRIES];
  G4double absorbLength[NUMENTRIES];

  for(int i=0; i<NUMENTRIES; ++i)
  {
      energy[i] = (12.405e2/(500-i)*eV);
	  rindex[i] = 1.58;
	  absorbLength[i] = 160.*cm;
  }

  G4double fast[NUMENTRIES] =  {0.0200, 0.0202, 0.0202, 0.0215, 0.0215,
                                0.0243, 0.0270, 0.0298, 0.0325, 0.0325,
                                0.0353, 0.0353, 0.0380, 0.0380, 0.0408,
                                0.0408, 0.0435, 0.0463, 0.0491, 0.0491,
                                0.0518, 0.0546, 0.0573, 0.0601, 0.0628,
                                0.0656, 0.0684, 0.0711, 0.0766, 0.0766,
                                0.0821, 0.0876, 0.0932, 0.0987, 0.1042,
                                0.1097, 0.1152, 0.1207, 0.1317, 0.1373,
                                0.1483, 0.1538, 0.1648, 0.1758, 0.1841,
                                0.1979, 0.2089, 0.2227, 0.2365, 0.2530,
                                0.2696, 0.2806, 0.2916, 0.3026, 0.3137,
                                0.3247, 0.3412, 0.3522, 0.3633, 0.3743,
                                0.3853, 0.3963, 0.4074, 0.4184, 0.4294,
                                0.4349, 0.4460, 0.4542, 0.4625, 0.4680,
                                0.4790, 0.4845, 0.4956, 0.5066, 0.5149,
                                0.5286, 0.5397, 0.5562, 0.5810, 0.5975,
                                0.6168, 0.6582, 0.6609, 0.7464, 0.8070,
                                0.8566, 0.9118, 0.9255, 0.9476, 0.9669,
                                0.9889, 0.9944, 0.9972, 0.9917, 0.9889,
                                0.9779, 0.9641, 0.9503, 0.9338, 0.9090,
                                0.8814, 0.8566, 0.8236, 0.7905, 0.7464,
                                0.6802, 0.6196, 0.5149, 0.4542, 0.4377,
                                0.3605, 0.2999, 0.2558, 0.2172, 0.1786,
                                0.1345, 0.0959, 0.0766, 0.0628, 0.0491,
                                0.0628};

  G4MaterialPropertiesTable* wtMPT = new G4MaterialPropertiesTable();

  wtMPT->AddProperty("RINDEX",        energy, rindex,       NUMENTRIES)->SetSpline(true);
  wtMPT->AddProperty("ABSLENGTH",     energy, absorbLength, NUMENTRIES)->SetSpline(true);
  wtMPT->AddProperty("FASTCOMPONENT", energy, fast,         NUMENTRIES)->SetSpline(true);
  wtMPT->AddProperty("SLOWCOMPONENT", energy, fast,         NUMENTRIES)->SetSpline(true);

#if 0
  wtMPT->AddConstProperty("SCINTILLATIONYIELD", 3./MeV);  // show a few scintillation
#else
  wtMPT->AddConstProperty("SCINTILLATIONYIELD", 10400./MeV);
#endif

  wtMPT->AddConstProperty("FASTTIMECONSTANT", 1.8*ns);
  wtMPT->AddConstProperty("SLOWTIMECONSTANT", 1.8*ns);
  wtMPT->AddConstProperty("YIELDRATIO", 1.); 
  wtMPT->AddConstProperty("RESOLUTIONSCALE", 1.0);

  //wtMPT->DumpTable();
  sc->SetMaterialPropertiesTable(wtMPT);

  // Set the Birks Constant for the plastic scintillator scintillator
  sc->GetIonisation()->SetBirksConstant(0.126*mm/MeV);

//
// Air
//
  G4double refractiveIndex2[121];

  for(int i = 0; i < 121; ++i)
  {
	  refractiveIndex2[i] = 1.;
  }

  G4MaterialPropertiesTable* airMPT = new G4MaterialPropertiesTable();
  airMPT->AddProperty("RINDEX", energy, refractiveIndex2, 121);

  //airMPT->DumpTable();

  air->SetMaterialPropertiesTable(airMPT);

//
// ------------- Volumes --------------

// The experimental Hall
//
  G4Box* expHall_box = new G4Box("World",fExpHall_x/2, fExpHall_y/2,fExpHall_z/2);
  G4LogicalVolume* expHall_log = new G4LogicalVolume(expHall_box,air,"World",0,0,0);
  G4VPhysicalVolume* expHall_phys = new G4PVPlacement(0,G4ThreeVector(),expHall_log,"World",0,false,0);

// The plastic scintillator 
//
  G4Box* scint_box = new G4Box("Scint", fScint_x/2, fScint_y/2, fScint_z/2);
  G4LogicalVolume* scint_log = new G4LogicalVolume(scint_box, sc, "Scint", 0, 0, 0);
  G4VPhysicalVolume* scint_phys = new G4PVPlacement(0,G4ThreeVector(), scint_log, "Scint", expHall_log,false,0);

// ------------- Surfaces --------------
//
// plastic scintillator
//
  G4OpticalSurface* opScintSurface = new G4OpticalSurface("ScintSurface");
  opScintSurface->SetType(dielectric_metal);
  opScintSurface->SetFinish(polished);
  opScintSurface->SetModel(unified);

  //G4LogicalBorderSurface* scintSurface = 
                    new G4LogicalBorderSurface("ScintSurface", scint_phys, expHall_phys, opScintSurface);

  const double lg_x = fScint_x;
  const double lg_y = fScint_y;
  const double lg_z = 0.01 * m;

  G4Box* lg_box = new G4Box("LightGuide", lg_x/2, lg_y/2, lg_z/2);
  G4LogicalVolume* lg_log = new G4LogicalVolume(lg_box, lg_mat, "LightGuide", 0, 0, 0);
  //G4VPhysicalVolume* lg_phys = 
	  new G4PVPlacement(0, G4ThreeVector(0, 0, fScint_z/2+lg_z/2), lg_log, "LightGuide", expHall_log, false, 0);

  //always return the physical World
  return expHall_phys;
}

void OpNoviceDetectorConstruction::SetThickness(G4double th)
{
	fScint_y    = th;
	G4RunManager::GetRunManager()->ReinitializeGeometry();
}

void OpNoviceDetectorConstruction::SetWidth(G4double wd)
{
	fScint_x    = wd;
	G4RunManager::GetRunManager()->ReinitializeGeometry();
}

void OpNoviceDetectorConstruction::SetLength(G4double len)
{
	fScint_z    = len;
	G4RunManager::GetRunManager()->ReinitializeGeometry();
}

G4double OpNoviceDetectorConstruction::GetThickness()
{
	return fScint_y;
}

G4double OpNoviceDetectorConstruction::GetWidth()
{
	return fScint_x;
}

G4double OpNoviceDetectorConstruction::GetLength()
{
	return fScint_z;
}
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
