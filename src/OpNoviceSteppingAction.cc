//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file OpNoviceSteppingAction.cc
/// \brief Implementation of the OpNoviceSteppingAction class

#include "OpNoviceSteppingAction.hh"

#include "G4Step.hh"
#include "G4Track.hh"
#include "G4OpticalPhoton.hh"

#include "G4Event.hh"
#include "G4RunManager.hh"
#include "g4root.hh"

#include "OpNoviceRunAction.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

OpNoviceSteppingAction::OpNoviceSteppingAction()
: G4UserSteppingAction()
{ 
  fScintillationCounter = 0;
  fCerenkovCounter      = 0;
  fEventNumber = -1;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

OpNoviceSteppingAction::~OpNoviceSteppingAction()
{ ; }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void OpNoviceSteppingAction::UserSteppingAction(const G4Step* step)
{
  //G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
				  
  std::vector<G4double>& timeOfPhotonVec   = OpNoviceRunAction::GetTimeOfPhotonVec();
  std::vector<G4int>&    atLGVec           = OpNoviceRunAction::GetAtLGVec();
  std::vector<G4double>& energyOfPhotonVec = OpNoviceRunAction::GetEnergyOfPhotonVec();
  std::vector<G4int>&    photonTypeVec     = OpNoviceRunAction::GetPhotonTypeVec(); 

  G4int eventNumber = G4RunManager::GetRunManager()-> GetCurrentEvent()->GetEventID();

  if (eventNumber != fEventNumber) {
     fEventNumber = eventNumber;
     fScintillationCounter = 0;
     fCerenkovCounter = 0;
  }

  G4Track* track = step->GetTrack();

  G4String ParticleName = track->GetDynamicParticle()-> GetParticleDefinition()->GetParticleName();

  // do we record the time of making a photon ?
  bool recordPhoton0 = true;

  // selection for the 'right' photon at z direction
  bool cutDIR = false;

  //const double maxTime = 0.7; //ns
  const double maxTime = 100000.; //ns

  if (ParticleName == "opticalphoton") 
  {
	  G4Track* optPhoton = track;
	  double optTime = optPhoton->GetGlobalTime()/CLHEP::ns;

	  const G4ThreeVector& photonMomentumDir0 = optPhoton->GetMomentumDirection();
	  double pz0 = photonMomentumDir0.z();

	  if(optTime > maxTime || (pz0 < 0 && cutDIR) ) 
	  {
	      optPhoton->SetTrackStatus(fKillTrackAndSecondaries);
	      return;
	  }

	  auto preVol  = step->GetPreStepPoint() ->GetPhysicalVolume();
	  auto postVol = step->GetPostStepPoint()->GetPhysicalVolume();

	  if(preVol != nullptr && postVol != nullptr)
	  {
		  G4String preVolName  = preVol->GetName();
		  G4String postVolName = postVol->GetName();

		  if(preVolName == "Scint" && postVolName == "LightGuide")
		  {
				  bool atLG = true; // does the photon reach the light guide ?
				  //// ---------- Fill photon -------------
				  const G4Track* opticalPhoton = track;
				  double opt = opticalPhoton->GetGlobalTime()/CLHEP::ns;

				  //analysisManager->FillNtupleDColumn(0, 0, opt);
				  //analysisManager->FillNtupleDColumn(0, 1, atLG);
				  timeOfPhotonVec.push_back(opt);
				  atLGVec.push_back(atLG);

#ifdef AdditionalPhotonInfo
				  double ope = opticalPhoton->GetTotalEnergy()/CLHEP::eV;
				  //double opl = CLHEP::hbarc * CLHEP::twopi/ope;

				  int photonType = 0;

				  //analysisManager->FillNtupleDColumn(0, 2, ope);
				  //analysisManager->FillNtupleDColumn(0, 3, photonType);
				  energyOfPhotonVec.push_back(ope);
                  photonTypeVec.push_back(photonType);
#endif
				  //analysisManager->AddNtupleRow(0);
				  // --------------------------------------------------
		  }
	  }

	  return;
  }

  const std::vector<const G4Track*>* secondaries = step->GetSecondaryInCurrentStep();


  if (secondaries->size()>0 && recordPhoton0) {
     for(unsigned int i=0; i<secondaries->size(); ++i) {
        if (secondaries->at(i)->GetParentID()>0) {
		   // if a optical photon, check the process
           if(secondaries->at(i)->GetDynamicParticle()->GetParticleDefinition()
               == G4OpticalPhoton::OpticalPhotonDefinition())
		   {
              if (secondaries->at(i)->GetCreatorProcess()->GetProcessName() == "Scintillation" ||
				  secondaries->at(i)->GetCreatorProcess()->GetProcessName() == "Cerenkov")
			  {

				  //// ---------- Fill photon -------------
				  const G4Track* opticalPhoton = secondaries->at(i);
				  double opt = opticalPhoton->GetGlobalTime()/CLHEP::ns;


				  bool atLG = false; // does the photon reach the light guide ?

				  timeOfPhotonVec.push_back(opt);
				  atLGVec.push_back(atLG);

#ifdef AdditionalPhotonInfo
				  double ope = opticalPhoton->GetTotalEnergy()/CLHEP::eV;

				  int photonType = 0;

				  if(opticalPhoton->GetCreatorProcess()->GetProcessName() == "Scintillation") 
					  photonType = 1;

				  if(opticalPhoton->GetCreatorProcess()->GetProcessName() == "Cerenkov")
					  photonType = 2;

				  energyOfPhotonVec.push_back(ope);
                  photonTypeVec.push_back(photonType);
#endif

				  fScintillationCounter++;
			  }

              if (secondaries->at(i)->GetCreatorProcess()->GetProcessName() == "Cerenkov")fCerenkovCounter++;
           }

        }
     }
  } // end if
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
