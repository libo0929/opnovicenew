Double_t dNdt(Double_t *t, Double_t *p)
{
	const double LEN = 1; // m
	const double rindex = 1.58;
	Float_t time = t[0];  // ps

	Double_t f = LEN * rindex / (TMath::C() * 1.e-12 * time * time);

	return f;
}

Double_t IntdNdtOverN(Double_t *t, Double_t *p)
{
	TF1 *f1 = new TF1("", dNdt, 5250, 6750);
	return f1->Integral(5250, t[0])/f1->Integral(5250, 6750);
}

Double_t Sig(Double_t *t, Double_t *para)
{
	Double_t vt = t[0];
	Double_t p = IntdNdtOverN(&vt, 0);
	Double_t Nthr = 2.;
	Double_t N = para[0];
	Double_t norm = 1./para[1];

	Double_t s = norm * N * dNdt(&vt, 0) * TMath::Factorial(N-1) / (TMath::Factorial(Nthr-1) * TMath::Factorial(N-Nthr)) 
		* TMath::Power(p, Nthr - 1) * TMath::Power(1 - p, N - Nthr);
	
	return s;
}

void timeResolution()
{
	TF1 *f1 = new TF1("", Sig, 5250, 7000, 2);
	f1->SetParameter(0, 50);
	f1->SetParameter(1, 1);

	//double funArea = f1->Integral(5250, 5500);
	//cout << funArea << endl;
	f1->SetParameter(1, 0.223049);

	f1->SetNpx(1000);
	f1->GetXaxis()->SetTitle("time (ps)");
	f1->GetYaxis()->SetTitle("prob. (1/ps)");
	f1->GetYaxis()->SetTitleOffset(1.5);
	f1->Draw();


#if 0
	TF1 *f2 = new TF1("", Sig, 5250, 7000, 1);
	f2->SetParameter(0, 25);
	f2->SetLineColor(kBlue);
	f2->SetNpx(200);
	f2->DrawNormalized("same");

	TF1 *f3 = new TF1("", Sig, 5250, 7000, 1);
	f3->SetParameter(0, 10);
	f3->SetLineColor(kGreen);
	f3->SetNpx(100);
	f3->DrawNormalized("same");

	TF1 *f4 = new TF1("", Sig, 5250, 7000, 1);
	f4->SetParameter(0, 5);
	f4->SetLineColor(kMagenta);
	f4->SetNpx(100);
	f4->DrawNormalized("same");
#endif
}
