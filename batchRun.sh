#!/usr/bin/env bash

exeName=OpNovice

nap()
{
        exeProc=`ps -U $USER | grep ${exeName} | wc -l`

        while [ $exeProc -ge 20 ]; do
			sleep 5
			exeProc=`ps -U $USER | grep ${exeName} | wc -l`
        done
}

thickness=1

while [ ${thickness} -le 10 ]; do
	echo "thickness: ${thickness}"
	sed -e "s;\ THICKNESS;\ ${thickness};" -e "s;\ FILENAME;\ muonData_${thickness}cm;" template.mac > run${thickness}.mac
	./${exeName} -m run${thickness}.mac > run${thickness}.log 2>&1 &
	nap

	let thickness+=1
done
