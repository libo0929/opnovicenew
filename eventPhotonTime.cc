void eventPhotonTime()
{
	gStyle->SetOptFit();

	//TH1F* hTime = new TH1F("time", "", 50, 0.21, 0.39);
	TH1F* hTime = new TH1F("time", "", 50, 0.65, 0.75);
	//TH1F* hTime = new TH1F("time", "", 50, 0, 1.2);
	//TH1F* hTime = new TH1F("time", "", 50, 1.041, 1.181);

	//TFile* f = new TFile("build_L10W25T5_5000/muonData_10cm.root");
	TFile* f = new TFile("build_L25W25T5_5000/muonData_1cm.root");
	//TFile* f = new TFile("build_L25W10_uniform/muonData_1cm.root");
	//TFile* f = new TFile("build_L40W10_5000/muonData_1cm.root");
    TNtuple* ntuple = (TNtuple*)f->Get("scint");

	std::vector<double>* timeOfPhotonVec = new std::vector<double>;
	std::vector<int>* atLGVec = new std::vector<int>;

	ntuple->SetBranchAddress("timeOfPhoton",  &timeOfPhotonVec);
	ntuple->SetBranchAddress("atLG",  &atLGVec);

    for(int i=0; i<ntuple->GetEntries(); i++)
    {
        ntuple->GetEntry(i);

		std::vector<double> timeOfPhotonAtLG;

		for(int i = 0; i < timeOfPhotonVec->size(); ++i)
		{
			double timeOfPhoton = timeOfPhotonVec->at(i);
			int    atLG         = atLGVec->at(i);

			if(atLG == 1) 
			{
				timeOfPhotonAtLG.push_back(timeOfPhoton);
			}
		}

		sort(timeOfPhotonAtLG.begin(), timeOfPhotonAtLG.end());

		//for(int np = 0; np < timeOfPhotonAtLG.size(); ++np)
		//for(int np = 0; np < 2; ++np)
		//{
		//	hTime->Fill(timeOfPhotonAtLG.at(np));
		//}
		hTime->Fill(timeOfPhotonAtLG.at(2));
	}

	TCanvas* can = new TCanvas("can", "", 800, 600);

	hTime->GetXaxis()->SetTitle("photon time (ns)");
	hTime->GetYaxis()->SetTitle("photon number");
	hTime->Draw();
	hTime->Fit("gaus");
    
	can->Print("time.pdf");
}
