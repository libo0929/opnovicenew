const double hc_const = TMath::HC()/TMath::Qe(); // eV * m

void makeHist(TH1F* h, TFile* f)
{
    TNtuple* ntuple = (TNtuple*)f->Get("scint");

    double timeOfPhoton;        
	double atLG;
	double energyOfPhoton;
	double photonType;

	ntuple->SetBranchAddress("timeOfPhoton",  &timeOfPhoton);
	ntuple->SetBranchAddress("atLG",  &atLG);
	ntuple->SetBranchAddress("energyOfPhoton",  &energyOfPhoton); // eV
	ntuple->SetBranchAddress("photonType",  &photonType);

    for(int i=0; i<ntuple->GetEntries(); i++)
    {
        ntuple->GetEntry(i);
        
		if(atLG == 0.) 
		{ 
			h->Fill(hc_const/energyOfPhoton * 1.e9);  // nm
			//cout << hc_const/energyOfPhoton << endl; 
		}
    }

	h->Scale(1./h->Integral());

	cout << "nPhotons: " << h->GetEntries() << endl;
}

void plotWL()
{
	gStyle->SetOptStat(0);

	TH1F* hWL = new TH1F("wl", "", 100, 380, 500);
	TFile* f1 = new TFile("build/muonData_1cm.root");

	makeHist(hWL, f1);

	hWL->SetLineWidth(2);
	hWL->Scale(1./hWL->GetBinContent(hWL->GetMaximumBin()));
	hWL->GetXaxis()->SetTitle("Wave length (nm)");
	hWL->GetYaxis()->SetTitle("Relative intensity");
	hWL->GetYaxis()->SetRangeUser(1.e-3, 1.005);
	hWL->SetLineColor(kRed);
	hWL->SetMarkerColor(kRed);
	hWL->SetMarkerSize(0);
	hWL->Draw();
}
